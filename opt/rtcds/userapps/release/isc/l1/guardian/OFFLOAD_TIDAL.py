from guardian import GuardState
import time
import cdsutils

nominal = 'OFFLOAD_ON'

class OFFLOAD_OFF(GuardState):
    index = 10
    request = True

class OFFLOAD_ON(GuardState):
    index = 100
    request = True

    def main(self):
        ctrl_prefix = 'ISI-ETM'+ARM+'_ST1_ISO_'+ARM
        if ARM == 'X':
            err_chan = 'SUS-ETMX_M0_TIDAL_L_OUTPUT'
        elif ARM == 'Y':
            err_chan = 'SUS-ETMY_M0_LOCK_L_OUTPUT'

        ezca[ctrl_prefix+'_OFFSET'] = 0
	ezca[ctrl_prefix+'_TRAMP'] = 10
        time.sleep(0.1)
	ezca.switch(ctrl_prefix, 'INPUT', 'ON')

        self.servo = cdsutils.Servo(ezca,
                                    channel=ctrl_prefix+'_OFFSET',
                                    readback=err_chan,
                                    gain=0.001,
                                    setval=0,
                                    )

    def run(self):
        self.servo.step()
            

edges = [
    ('OFFLOAD_OFF', 'OFFLOAD_ON'),
    ('OFFLOAD_ON', 'OFFLOAD_OFF')
    ]
